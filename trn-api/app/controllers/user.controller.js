const connection = require('./mysql.controller');
const mysql = require('mysql');
const uuid = require('uuid');

// Registration
exports.register = (req, res) => {

    const salt = uuid.v4().toString();

    // Validate request
    if (!req.body.Email) {
        return res.status(400).send({
            message: "Email is required"
        });
    }

    if (!req.body.Password) {
        return res.status(400).send({
            message: "Password is required"
        });
    }

    if (!req.body.PasswordConfirmation) {
        return res.status(400).send({
            message: "Please confirm the password"
        });
    }

    if (req.body.Password != req.body.PasswordConfirmation) {
        return res.status(400).send({
            message: "Passwords do not match"
        });
    }

    let checkQuery = `SELECT UserId from \`user\` WHERE Email=${mysql.escape(req.body.Email)}`;

    console.log(checkQuery);

    connection.query(checkQuery, function (objCheck) {
        console.log(objCheck.response);
        if (objCheck.response && objCheck.response && objCheck.response.length) {
            return res.status(400).send({
                message: `The email ${req.body.Email} has already been used. Please use another email.`
            });
        }

        let query = `INSERT INTO \`user\` 
                (FirstName, LastName, FullName, Gender, Email, Password, Salt)
                VALUES (
                    ${mysql.escape(req.body.FirstName)},
                    ${mysql.escape(req.body.LastName)},
                    ${mysql.escape(req.body.FirstName + ' ' + req.body.LastName)},
                    ${mysql.escape(req.body.Gender)},
                    ${mysql.escape(req.body.Email)},
                    MD5(CONCAT_WS(${mysql.escape(req.body.Password)}, '|', ${mysql.escape(salt)})),
                    ${mysql.escape(salt)}
                )`;

        console.log(query);

        connection.query(query, function (objInsert) {

            let selectQuery = `SELECT * FROM \`user\` u
                            WHERE u.Email = '${req.body.Email}' 
                            AND u.Password = MD5(CONCAT_WS('${req.body.Password}', '|', u.Salt))`;


            connection.query(selectQuery, function (objSelect) {

                return res.status(200).send({
                    success: true,
                    message: null,
                    data: { insertResponse: objInsert.response, userData: objSelect.response[0] }
                });
            });
        });
    })

}

// Logging into the portal
exports.login = (req, res) => {

    // Validate request
    if (!req.body.Email) {
        return res.status(400).send({
            message: "Email is required"
        });
    }

    if (!req.body.Password) {
        return res.status(400).send({
            message: "Password is required"
        });
    }

    let query = `SELECT *
                FROM \`user\` u
                JOIN user_role_map urm
                ON u.UserId = urm.UserId
                WHERE u.Email = '${req.body.Email}' 
                AND u.Password = MD5(CONCAT_WS('${req.body.Password}', '|', u.Salt))`;

    console.log(query);

    connection.query(query, function (obj) {
        if (obj.response.length) {
            var userInfo = obj.response[0];

            res.status(200).send({
                success: true,
                message: null,
                data: userInfo
            });
        } else
            res.status(400).send({
                success: false,
                message: `Invalid login`,
                data: null
            });
    });


}

// Reset all passwords (for all users)
exports.updateallpasswords = (req, res) => {
    let query = '';

    for (let i = 0; i < 100; i++) {
        query = `UPDATE employee SET Salt = UUID() WHERE EmployeeId = ${i};
                UPDATE employee SET Password = MD5(CONCAT_WS('1234', '|', Salt)) WHERE EmployeeId = ${i}`;

        connection.query(query, function (obj) {
            console.log(query);
        });
    }

    res.send({
        s: 'success'
    });
}

// Reset all passwords (for all users)
exports.resetpassword = (req, res) => {

    console.log(req.body);

    if (!req.body.Email) {
        return res.status(400).send({
            success: false,
            message: 'Email is required',
            data: null
        })
    }

    if (req.body.Password) {

        if (!req.body.OldPassword) {
            return res.status(400).send({
                success: false,
                message: 'Old Password is required',
                data: null
            })
        }

        let checkPasswordQuery = `SELECT Count(EmployeeId) empCount FROM employee 
                                    WHERE Email = '${req.body.Email}' 
                                    AND Password = MD5(CONCAT_WS('${req.body.Password}', '|', Salt))`;

        let oldPasswordCheckFailed = false;
        connection.query(checkPasswordQuery, function (objCheckPassword) {
            console.log(objCheckPassword);

            if (!objCheckPassword.response[0].empCount) {
                oldPasswordCheckFailed = true;
                return;
            }
        });

        if (oldPasswordCheckFailed) {
            return res.status(400).send({
                success: false,
                message: 'Old Password is incorrect',
                data: null
            });
        }

    }

    // let checkQuery = `SELECT Count(EmployeeId) empCount FROM employee WHERE Email = '${req.body.Email}'`;

    // connection.query(checkQuery, function (objCheck) {

    //     // console.log(objCheck);res.send({message:'test'});return;


    //     if (!objCheck || !objCheck.response[0].empCount) {
    //         res.status(400).send({
    //             success: false,
    //             message: `Email address not found`,
    //             data: null
    //         });
    //         return;
    //     }
    // });



    let letter_count = 8;
    let tempPassword = '';
    for (let l = 0; l < letter_count; l++) {
        let ucase = Math.floor((Math.random() * 26) + 97);
        let lcase = Math.floor((Math.random() * 26) + 65);
        let num = Math.floor((Math.random() * 10) + 48);
        let arr = [ucase, lcase, num];
        let index = Math.floor(Math.random() * 3);
        let charCode = arr[index];
        tempPassword += String.fromCharCode(charCode);
    }

    let password = !req.body.Password ? tempPassword : req.body.Password;

    console.log(password);


    let query = `UPDATE employee SET Salt = UUID() WHERE Email = '${req.body.Email}';
                UPDATE employee SET Password = MD5(CONCAT_WS('${password}', '|', Salt)) WHERE Email = '${req.body.Email}';`;


    console.log(query);


    let isReset = !req.body.Password;
    let currentEmail = req.body.Email;

    connection.query(query, function (obj) {

        // send out a mail, only for password resets
        if (isReset) {
            let emailObj = {
                message: {
                    from: "\"🔔 SEPM Admin\"  sepm.management@gmail.com",
                    to: [
                        currentEmail
                    ],
                    subject: "Password Reset",
                    html: `<div 
                            style="
                                width:55%;
                                padding:35px;
                                font-family:Verdana,Tahoma,Arial,sans-serif;
                                background:#eee;
                                border:1px solid #777;
                                margin:10px auto
                            ">
                            <h1 style="color:#7ad;font-family:Tahoma,Arial,sans-serif;font-weight:200">Your password has been reset</h1>
                            <p>
                                You (or someone else) has requested a password reset for your SEPM account. Please log in to SEPM with the password 
                                <code style="font-size:20px;color:#699;font-weight:bold">${tempPassword}</code>. Please remember to change your password afterwards.
                            </p>
                        </div>`,
                }
            };

            // mail.sendgmailfunction(emailObj);
        }

        res.send({
            success: obj.error == null,
            message: obj.error || `Success`,
            data: obj.response
        });

    });

}


