-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.5.4-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for trndb
DROP DATABASE IF EXISTS `trndb`;
CREATE DATABASE IF NOT EXISTS `trndb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `trndb`;

-- Dumping structure for table trndb.module
DROP TABLE IF EXISTS `module`;
CREATE TABLE IF NOT EXISTS `module` (
  `ModuleId` int(11) NOT NULL AUTO_INCREMENT,
  `ModuleName` tinytext COLLATE utf8mb4_bin NOT NULL,
  `ModuleDescription` text COLLATE utf8mb4_bin DEFAULT NULL,
  `Slug` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`ModuleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table trndb.module: ~0 rows (approximately)
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
/*!40000 ALTER TABLE `module` ENABLE KEYS */;

-- Dumping structure for table trndb.module_role
DROP TABLE IF EXISTS `module_role`;
CREATE TABLE IF NOT EXISTS `module_role` (
  `ModuleRoleId` int(11) NOT NULL AUTO_INCREMENT,
  `ModuleRoleName` tinytext COLLATE utf8mb4_bin DEFAULT NULL,
  `ModuleRoleCode` varchar(3) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ModuleRoleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table trndb.module_role: ~0 rows (approximately)
/*!40000 ALTER TABLE `module_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_role` ENABLE KEYS */;

-- Dumping structure for table trndb.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `RoleId` int(11) NOT NULL AUTO_INCREMENT,
  `RoleName` tinytext COLLATE utf8mb4_bin DEFAULT NULL,
  `RoleCode` varchar(3) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`RoleId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table trndb.role: ~3 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`RoleId`, `RoleName`, `RoleCode`) VALUES
	(1, 'System Admin', 'SA'),
	(2, 'Instructor', 'IN'),
	(3, 'Student', 'ST');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping structure for table trndb.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `UserId` bigint(20) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(60) NOT NULL,
  `MiddleName` varchar(60) DEFAULT NULL,
  `LastName` varchar(60) NOT NULL,
  `FullName` varchar(200) DEFAULT NULL,
  `Gender` enum('M','F','N') DEFAULT NULL,
  `Email` varchar(300) NOT NULL DEFAULT 'none@blank.org',
  `Password` varchar(32) NOT NULL DEFAULT '1234',
  `Salt` varchar(36) NOT NULL DEFAULT '',
  `CreatedBy` bigint(20) DEFAULT NULL,
  `CreatedDateTime` datetime DEFAULT current_timestamp(),
  `LastUpdatedBy` bigint(20) DEFAULT NULL,
  `LastUpdatedDateTime` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `Deleted` enum('Y','N') DEFAULT 'N',
  `DeletedBy` bigint(20) DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`UserId`) USING BTREE,
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Dumping data for table trndb.user: ~8 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`UserId`, `FirstName`, `MiddleName`, `LastName`, `FullName`, `Gender`, `Email`, `Password`, `Salt`, `CreatedBy`, `CreatedDateTime`, `LastUpdatedBy`, `LastUpdatedDateTime`, `Deleted`, `DeletedBy`, `DeletedDateTime`) VALUES
	(1, 'Debadeep', NULL, 'Sen', 'Debadeep Sen', 'M', 'debadeep.sen@gmail.com', '066095301b66046b5b72a13da796ce41', '02df1aa0-3143-11ea-8d17-8cec4bb3f382', NULL, '2020-10-14 19:18:20', NULL, '2020-12-04 16:33:18', 'N', NULL, NULL),
	(2, 'Demo', NULL, 'User', 'Bot Demo', 'N', 'demo@user.us', '066095301b66046b5b72a13da796ce41', '02df1aa0-3143-11ea-8d17-8cec4bb3f382', NULL, '2020-12-04 16:31:19', NULL, '2021-01-22 11:15:13', 'N', NULL, NULL),
	(3, 'Demo', NULL, 'User 2', 'Demo User 2', 'F', 'de@mo.us', 'e33fb4c1fbb188735ddcdb3ba1f1cebb', '6EE02CD1-C277-4D9F-ACCD-23834BAB6FE4', NULL, '2020-12-04 16:44:32', NULL, NULL, 'N', NULL, NULL),
	(4, 'Alexander', NULL, 'Tiedemann', 'Alexander Tiedemann', 'M', 'alex@winden.org', '978d58a16f2b8a70cc172fb5d9516ed4', '6013afad-f296-4643-9df1-537f986399a7', NULL, '2020-12-04 16:59:02', NULL, NULL, 'N', NULL, NULL),
	(5, 'Debadeep', NULL, 'Sen', 'Debadeep Sen', 'M', 'debadeepsen19@gmail.com', '93875bf713c4189a93544b6c4960b457', '0b2aaf9d-aa2b-4710-97db-f350d410b6a3', NULL, '2020-12-05 14:46:49', NULL, NULL, 'N', NULL, NULL),
	(10, 'Debadeep', NULL, 'Sen', 'Debadeep Sen', 'M', 'ds1@gmail.com', '8c482618cfc0852189d79f85f62914c0', '4a76c13d-2075-4695-bd15-8cd307b96a6e', NULL, '2020-12-05 14:52:41', NULL, NULL, 'N', NULL, NULL),
	(11, 'Jonas', NULL, 'Kahnwald', 'Jonas Kahnwald', 'M', 'jk@winden.org', 'c6b90788946ded166b12d9f987bbc334', '2bca31e2-a256-4c0f-91d3-35873a4ee2b2', NULL, '2020-12-05 14:55:05', NULL, NULL, 'N', NULL, NULL),
	(16, 'Krishna', NULL, 'Attenborough', 'Krishna Attenborough', 'F', 'kat@online.uk', '7f8157bc405d99bd7044beb08d55336d', '414bcdea-bafc-4f6e-9528-86900489540b', NULL, '2020-12-05 15:44:30', NULL, NULL, 'N', NULL, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table trndb.user_module_map
DROP TABLE IF EXISTS `user_module_map`;
CREATE TABLE IF NOT EXISTS `user_module_map` (
  `UserModuleMapId` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserId` bigint(20) DEFAULT NULL,
  `ModuleId` int(11) DEFAULT NULL,
  `ModuleRoleId` int(11) DEFAULT NULL,
  PRIMARY KEY (`UserModuleMapId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table trndb.user_module_map: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_module_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_module_map` ENABLE KEYS */;

-- Dumping structure for table trndb.user_role_map
DROP TABLE IF EXISTS `user_role_map`;
CREATE TABLE IF NOT EXISTS `user_role_map` (
  `UserRoleMapId` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserId` bigint(20) NOT NULL DEFAULT 0,
  `RoleId` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`UserRoleMapId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table trndb.user_role_map: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_role_map` DISABLE KEYS */;
INSERT INTO `user_role_map` (`UserRoleMapId`, `UserId`, `RoleId`) VALUES
	(1, 1, 2),
	(2, 4, 3),
	(3, 11, 3),
	(4, 16, 3);
/*!40000 ALTER TABLE `user_role_map` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
